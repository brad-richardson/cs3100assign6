#include <iostream>
#include <vector>
#include "stdlib.h"
#include "simulator.hpp"

int main(int argc, char* argv[])
{
	int cpu;
	int ioDevices;
	if (argc < 3)
	{
		std::cout << "How many processors? ";
		std::cin >> cpu;
		
		std::cout << "How many IO devices? ";
		std::cin >> ioDevices;
	}
	else
	{
		cpu = atoi(argv[1]);
		ioDevices = atoi(argv[2]);
	}

	// std::default_random_engine generator;
	// std::uniform_real_distribution<double> distribution(0,10);
	// for(int i = 0; i < 10; ++i)
	// {
	// 	std::cout << distribution(generator) << " ";
	// }

	Simulator sim(cpu, ioDevices);
	std::cout << sim.toString();

	return 0;
}