#include <queue>
#include <string>
#include <sstream>
#include <memory>
#include <random>

typedef std::pair<double, double> Task;

class Process
{
private:
	std::deque<Task> tasks;
	std::deque<int> ioDeviceToUse;
	bool cpuBound;
	int ioDevices;
	float latency = -1; //time between creation and completion
	float response = -1; //time between init and first cycle completion
	float initTime = -1;

public:
	Process(int numOfTasks, bool cpuBound, int ioDevices);
	// {
	// 	generateValues(numOfTasks);
	// }

	// void generateValues(int numOfTasks)
	// {
	// 	int cpuMin, cpuMax, ioMin, ioMax;
	// 	//if it is cpu bound, generate cpu heavy tasks, light on io and vice-versa
	// 	std::default_random_engine generator;
	// 	if(cpuBound)
	// 	{
	// 		cpuMin = 5;
	// 		cpuMax = 50;
	// 		ioMin = 0;
	// 		ioMax = 5;
	// 	}
	// 	else
	// 	{
	// 		cpuMin = 0;
	// 		cpuMax = 5;
	// 		ioMin = 5;
	// 		ioMax = 50;
	// 	}
	// 	//different distributions for different ranges of numbers
	// 	std::uniform_real_distribution<double> cpuDist(cpuMin, cpuMax);
	// 	std::uniform_real_distribution<double> ioDist(ioMin, ioMax);
	// 	std::uniform_int_distribution<int> ioDeviceDist(0,ioDevices-1);
	// 	for(auto i = 0; i < numOfTasks; ++i)
	// 	{
	// 		double cpuTime = cpuDist(generator);
	// 		double ioTime = ioDist(generator);
	// 		double ioDevice = ioDeviceDist(generator);
	// 		tasks.push_back(Task(cpuTime, ioTime));
	// 		ioDeviceToUse.push_back(ioDevice);
	// 	}
	// }

	Task getTask()
	{
		return tasks.front();
	}

	bool isCPUBound()
	{
		return cpuBound;
	}

	std::string toString()
	{
		std::stringstream ss;
		for(auto i = 0; i < (int)tasks.size(); ++i)
		{

			ss << "Task " << i << ": " << tasks[i].first << "," << tasks[i].second 
				<< "(" << ioDeviceToUse[i] << ")" << std::endl;
		}
		return ss.str();
	}


};