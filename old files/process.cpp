#include "process.hpp"

Process::Process(int numOfTasks, bool cpuBound, int ioDevices)
	 : cpuBound(cpuBound), ioDevices(ioDevices)
{
	int cpuMin, cpuMax, ioMin, ioMax;
	//if it is cpu bound, generate cpu heavy tasks, light on io and vice-versa
	std::default_random_engine generator;
	if(cpuBound)
	{
		cpuMin = 5;
		cpuMax = 50;
		ioMin = 0;
		ioMax = 5;
	}
	else
	{
		cpuMin = 0;
		cpuMax = 5;
		ioMin = 5;
		ioMax = 50;
	}
	//different distributions for different ranges of numbers
	std::uniform_real_distribution<double> cpuDist(cpuMin, cpuMax);
	std::uniform_real_distribution<double> ioDist(ioMin, ioMax);
	std::uniform_int_distribution<int> ioDeviceDist(0,ioDevices-1);
	for(auto i = 0; i < numOfTasks; ++i)
	{
		double cpuTime = cpuDist(generator);
		double ioTime = ioDist(generator);
		double ioDevice = ioDeviceDist(generator);
		tasks.push_back(Task(cpuTime, ioTime));
		ioDeviceToUse.push_back(ioDevice);
	}
}