#include <iostream>
#include <queue>
#include <string>
#include <sstream>
#include <random>
#include "process.hpp"

class Simulator {
private:
	int numCPU; //cpu devices
	int numIO; //io devices
	int numProcesses;
	float contextSwitchCost;
	float cpuBoundPct; //percentage of processes which are cpu bound
	float freq; // how often new processes are "generated"
	std::deque<std::shared_ptr<Process>> processes;
	std::default_random_engine generator;
	std::uniform_int_distribution<int> taskDist;
	std::bernoulli_distribution cpuBoundDist;

	// TODO - implement std::greater for Process class
	// std::priority_queue<float, std::vector<float>, std::greater<float> > cpuQueue;
	// std::priority_queue<float, std::vector<float>, std::greater<float> > ioQueue;


public:
	Simulator(int cpu = 2, int io = 2, int proc = 100, float context = 1,
		float cpuBound = .5, float freq = 1)
		: numCPU(cpu), numIO(io), numProcesses(proc), contextSwitchCost(context),
		cpuBoundPct(cpuBound), freq(freq)
	{		
		taskDist = std::uniform_int_distribution<int>(2,15);
		cpuBoundDist = std::bernoulli_distribution(cpuBoundPct);
		//generate processes 
		for(int i = 0; i < numProcesses; ++i)
		{
			Process p = Process(taskDist(generator), cpuBoundDist(generator), numIO);
			processes.push_back(std::make_shared<Process>(p));
		}
		for(std::shared_ptr<Process> pPtr : processes)
		{
			Process * p = pPtr.get();
			if(p->isCPUBound())
			{
				++cpuBound;
			}
			std::cout << p->toString();
		}
		std::cout << "CPU bound: " << cpuBound << std::endl;
		std::cout << "IO bound: " << numProcesses-cpuBound << std::endl;
	}


	std::string toString()
	{
		std::stringstream ss;
		ss << "CPUs: " << numCPU << std::endl;
		ss << "IO devices: " << numIO << std::endl;
		ss << "Processes: " << numProcesses << std::endl;
		ss << "Context switch cost: " << contextSwitchCost << std::endl;
		ss << "CPU bound process percent: " << cpuBoundPct << std::endl;
		ss << "Process generation frequency: " << freq << std::endl;
		return ss.str();
	}
};