#ifndef FIFOSIMULATOR_HPP
#define FIFOSIMULATOR_HPP

#include <iostream>
#include <deque>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <algorithm>
#include "process.hpp"
#include "tools.hpp"

class FIFOSimulator {
private:
	int numCPU;
	int numIO;
	int processesSpawned = 0;
	//determines how many tasks(or bursts) per process
	int minTasks = 2;
	int maxTasks = 10;

	double currTime;
	double endTime;

	double contextSwitchCost;
	double freq; 
	double timeToNextSpawn;
	double cpuBoundPct;
	std::vector<double> utilization;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> taskDist;
	std::bernoulli_distribution cpuBoundDist;

	std::deque<Process> readySet;
	std::vector<Process> executeSet;
	std::vector<std::deque<Process>> waitSet;
	std::vector<Process> doneSet;
	
public:
	FIFOSimulator(int numCPU, int numIO, double endTime, 
		double contextSwitchCost, double freq, double cpuBoundPct);

	void runSim();

	double findNextEvent();

	std::string printStats();

	std::string toString();

};

#endif