#include "fifosimulator.hpp"

FIFOSimulator::FIFOSimulator(int numCPU, int numIO, double endTime, 
		double contextSwitchCost, double freq, double cpuBoundPct)
		: numCPU(numCPU), numIO(numIO), endTime(endTime), 
		contextSwitchCost(contextSwitchCost), freq(freq), cpuBoundPct(cpuBoundPct)
{		
	currTime = 0;
	timeToNextSpawn = freq;
	taskDist = std::uniform_int_distribution<int>(minTasks,maxTasks);
	if (cpuBoundPct > 1 || cpuBoundPct < 0)
	{
		std::cout << "Invalid CPU bound pct, using 0.5..." << std::endl;
		cpuBoundPct = 0.5;
	}
	cpuBoundDist = std::bernoulli_distribution(cpuBoundPct);
	waitSet.resize(numIO);
	runSim();
	std::cout << printStats();
}

void FIFOSimulator::runSim()
{
	while(currTime < endTime)
	{
		double nextEventTime = findNextEvent();

		//change all values as if time has passed
		currTime += nextEventTime;
		timeToNextSpawn -= nextEventTime;
		for(Process &p : executeSet)
		{
			p.subtractCPUTime(nextEventTime);
		}
		for(std::deque<Process> &set : waitSet)
		{
			if (set.size() > 0)
				set.at(0).subtractIOTime(nextEventTime);
		}

		//check for new process to spawn and put on ready queue
		if(timeToNextSpawn <= 0)
		{
			processesSpawned++;
			readySet.push_back(Process(taskDist(generator), cpuBoundDist(generator), numIO, currTime));
			timeToNextSpawn = freq; //reset timer
		}
		//measures current cpu usage
		utilization.push_back(executeSet.size() / numCPU);
		//check for open cpu
		//if open cpu, add cpu time and move to cpu queue
		if((int)executeSet.size() < numCPU && !readySet.empty())
		{
			Process p = readySet.front();
			p.addCPUTime(contextSwitchCost);
			executeSet.push_back(p);
			readySet.pop_front();
		}

		//if cpu process is done, move to io queue
		for(int i = 0; i < (int)executeSet.size(); ++i)
		{
			Process p = executeSet[i];
			double timeLeft = p.getCurrCPU();
			if(timeLeft <= 0)
			{
				waitSet.at(p.getCurrIODevice()).push_back(p);
				executeSet.erase(executeSet.begin()+i);
			}
		}

		//if io is done then cycle process
		//if process is done then move to finish set
		//otherwise move back to ready set
		for(int i = 0; i < (int)waitSet.size(); ++i)
		{
			//only process we are interested in is the first one in each io device queue
			if (waitSet.at(i).size() > 0)
			{
				Process p = waitSet.at(i).at(0);
				double timeLeft = p.getCurrIO();
				if (timeLeft <= 0)
				{
					p.doCycle(currTime);
					if (p.isDone())
					{
						doneSet.push_back(p);
					}
					else
					{
						readySet.push_back(p);
					}
					waitSet.at(i).pop_front();
				}
			}
		}
	}
	return;
}

//finds next(smallest) event to "run"
double FIFOSimulator::findNextEvent()
{
	//finds smallest of all events in cpu, io and next spawn time
	double shortestCPU, shortestIO;
	shortestCPU = shortestIO = std::numeric_limits<double>::max();
	//finds smallest and compares all three
	for(Process p : executeSet)
	{
		double newVal = p.getCurrCPU();
		if(newVal < shortestCPU)
		{
			shortestCPU = newVal;
		}
	}
	for(int i = 0; i < (int)waitSet.size() && waitSet.at(i).size() > 0; ++i)
	{
		double newVal = waitSet.at(i).at(0).getCurrIO();
		if(newVal < shortestIO)
		{
			shortestIO = newVal;
		}
	}
	return std::min(shortestCPU, std::min(shortestIO, timeToNextSpawn));
}

//returns string with response, latency, utilization, and throughput
std::string FIFOSimulator::printStats()
{
	std::stringstream ss;
	ss << std::endl << "Stats: " << std::endl;
	ss << "Processes finished/created: " << doneSet.size() << "/" << processesSpawned << std::endl;
	std::vector<double> responseVals;
	for (Process p : readySet)
	{
		responseVals.push_back(p.getResponse());
	}
	for (Process p : executeSet)
	{
		responseVals.push_back(p.getResponse());
	}
	for (std::deque<Process> &set : waitSet)
	{
		for (Process p : set)
		{
			responseVals.push_back(p.getResponse());
		}
	}
	for (Process p : doneSet)
	{
		responseVals.push_back(p.getResponse());
	}
	if ((int) responseVals.size() > 0 && mean(responseVals )> 0)
	{
		ss << "Response - avg: " << mean(responseVals)
			<< " min: " << findMin(responseVals) << " max: " << findMax(responseVals)
			<< " sd: " << stdDev(responseVals) << std::endl;
	}
	else
	{
		ss << "Response - no processes completed any cycles" << std::endl;
	}
	std::vector<double> latencyVals;
	for (Process p : doneSet)
	{
		latencyVals.push_back(p.getLatency());
	}
	if (latencyVals.size() > 0)
	{
		ss << "Latency - avg: " << mean(latencyVals)
			<< " min: " << findMin(latencyVals) << " max: " << findMax(latencyVals)
			<< " sd: " << stdDev(latencyVals) << std::endl;
	}
	else
	{
		ss << "Latency - no processes finished" << std::endl;
	}

	ss << "CPU utilization: " << mean(utilization)*100 << "%" << std::endl;
	ss << "Throughput: " << doneSet.size()*100/endTime << " processes/100 time" << std::endl;
	ss << "Processes still in ready set: " << readySet.size() << std::endl;
	ss << "Processes still in execute set: " << executeSet.size() << std::endl;
	int processesInWaitSet = 0;
	for (std::deque<Process> &set : waitSet)
	{
		for (Process p : set)
		{
			processesInWaitSet++;
		}
	}
	ss << "Processes still in wait set: " << processesInWaitSet << std::endl << std::endl;
	
	return ss.str();
}

std::string FIFOSimulator::toString()
{
	std::stringstream ss;
	ss << "Simulator settings: " << std::endl;
	ss << "CPUs: " << numCPU << std::endl;
	ss << "IO devices: " << numIO << std::endl;
	ss << "End time: " << endTime << std::endl;
	ss << "Context switch cost: " << contextSwitchCost << std::endl;
	ss << "Process generation frequency: " << freq << std::endl;
	ss << "CPU bound task ratio: " << cpuBoundPct << std::endl;
	return ss.str();
}