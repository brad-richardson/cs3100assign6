#include "process.hpp"

Process::Process(int numOfTasks, bool cpuBound, int ioDevices, double currTime)
	 : cpuBound(cpuBound), ioDevices(ioDevices), initTime(currTime)
{
	int cpuMin, cpuMax, ioMin, ioMax;
	//if it is cpu bound, generate cpu heavy tasks, light on io and vice-versa
	std::default_random_engine generator;
	generator.seed((unsigned long)currTime*10000); 
	//can change these values to make longer or shorter processes
	if(cpuBound)
	{
		cpuMin = 10;
		cpuMax = 50;
		ioMin = 0;
		ioMax = 5;
	}
	else
	{
		cpuMin = 0;
		cpuMax = 5;
		ioMin = 10;
		ioMax = 50;
	}
	//different distributions for different ranges of numbers
	std::uniform_real_distribution<double> cpuDist(cpuMin, cpuMax);
	std::uniform_real_distribution<double> ioDist(ioMin, ioMax);
	std::uniform_int_distribution<int> ioDeviceDist(0,ioDevices-1);
	for(int i = 0; i < numOfTasks; ++i)
	{
		double cpuTime = cpuDist(generator);
		double ioTime = ioDist(generator);
		int ioDevice = ioDeviceDist(generator);
		tasks.push_back(Task(cpuTime, ioTime));
		ioDeviceToUse.push_back(ioDevice);
	}
}

Task Process::getTask()
{
	return tasks.front();
}
void Process::subtractCPUTime(double time)
{
	tasks.front().first -= time;
}
void Process::addCPUTime(double time)
{
	tasks.front().first += time;
}
void Process::subtractIOTime(double time)
{
	tasks.front().second -= time;
}
double Process::getCurrCPU()
{
	return tasks.front().first;
}
double Process::getCurrIO()
{
	return tasks.front().second;
}
int Process::getCurrIODevice()
{
	return ioDeviceToUse.front();
}
double Process::getResponse()
{
	return response;
}
double Process::getLatency()
{
	return latency;
}
bool Process::isCPUBound()
{
	return cpuBound;
}
bool Process::isDone()
{
	return ((int) tasks.size() == 0);
}

//after each cpu, io/wait cycle, removes task and does any necessary calculations
void Process::doCycle(double currTime)
{
	ioDeviceToUse.pop_front();
	if(response == -1)
	{
		response = currTime - initTime;
		tasks.pop_front();
	}
	else
	{
		tasks.pop_front();
	}
	if(tasks.size() == 0)
	{
		latency = currTime - initTime;
	}
}

std::string Process::toString()
{
	std::stringstream ss;
	for(int i = 0; i < (int)tasks.size(); ++i)
	{

		ss << "Task " << i << ": " << tasks[i].first << "," << tasks[i].second 
			<< "(" << ioDeviceToUse[i] << ")" << std::endl;
	}
	return ss.str();
}